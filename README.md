# Практическая работа №2
## О программе
Допустим, есть умный дом, в котором есть лампа,  которая может существовать в единственном виде (паттерн Одиночка), которая управляется главным устройствомб например, смартфоном (паттерн Команда). Для этого нужно иметь возможность подключаться к устройствам дома по различным протоколам, например, Wi-Fi и Bluetooth. (паттерн Адаптер).
## Одиночка
Singleton — порождающий шаблон проектирования, гарантирующий, что в однопоточном приложении будет единственный экземпляр некоторого класса, и предоставляющий глобальную точку доступа к этому экземпляру.
## Команда
Facade — Шаблон «Команда» позволяет инкапсулировать действия в объекты. Ключевая идея — предоставить средства отделения клиента от получателя.
## Адаптер
Шаблон проектирования «Адаптер» позволяет использовать интерфейс существующего класса как другой интерфейс. Этот шаблон часто применяется для обеспечения работы одних классов с другими без изменения их исходного кода.
## UML программы
```plantuml
@startuml

skin rose

title Classes - Class Diagram


class MainDevice {
  -MainDevice instance
  -static MainDevice()
  -static MainDevice GetInstance()
  +ExecuteCommand(IDeviceCommand command)
}

Interface IDeviceAdapter{
    Connect(int deviceId)
}

class WiFiAdapter{
  +Connect(int deviceId)
}

class BluetoothAdapter{
  +Connect(int deviceId)
}


class DeviceAdapter{
  -IDeviceAdapter adapter
  -DeviceAdapter(string adapterType)
  +Connect(int deviceId)
}

Interface IDeviceCommand{
    Execute();
}

class TurnOnCommand{
  -int deviceId;
  +TurnOnCommand(int deviceId)
  +Execute()
}

class TurnOffCommand{
  -int deviceId;
  +TurnOnCommand(int deviceId)
  +Execute()
}

WiFiAdapter --> MainDevice 
BluetoothAdapter -> MainDevice 
WiFiAdapter <- DeviceAdapter
BluetoothAdapter <- DeviceAdapter
IDeviceAdapter -> WiFiAdapter
IDeviceAdapter -> BluetoothAdapter
IDeviceCommand --> TurnOnCommand
IDeviceCommand -> TurnOffCommand
TurnOffCommand -> DeviceAdapter
TurnOnCommand -> DeviceAdapter
@enduml
```
