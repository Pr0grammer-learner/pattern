﻿using System;

namespace ConsoleApp4
{

    public class MainDevice
    {
        private static MainDevice instance;

        private MainDevice()
        {
            Console.WriteLine("Лампа");
            // Инициализация главного устройства
        }

        public static MainDevice GetInstance()
        {
            if (instance == null)
            {
                instance = new MainDevice();
            }
            else
            {
                Console.WriteLine("Лампа уже существует");
            }
            return instance;
        }

        public void ExecuteCommand(IDeviceCommand command)
        {
            command.Execute();
        }
    }

    public interface IDeviceAdapter
    {
        void Connect(int deviceId);
    }

    public class WiFiAdapter : IDeviceAdapter
    {
        public void Connect(int deviceId)
        {
            Console.WriteLine("Реализация подключения к устройству по протоколу Wi-Fi");
        }
    }

    public class BluetoothAdapter : IDeviceAdapter
    {
        public void Connect(int deviceId)
        {
            Console.WriteLine("Реализация подключения к устройству по протоколу Bluetooth");
        }
    }

    public class DeviceAdapter
    {
        private IDeviceAdapter adapter;

        public DeviceAdapter(string adapterType)
        {
            if (adapterType == "wifi")
            {
                adapter = new WiFiAdapter();
            }
            else if (adapterType == "bluetooth")
            {
                adapter = new BluetoothAdapter();
            }
            else
            {
                throw new Exception("Неизвестный тип адаптера.");
            }
        }

        public void Connect(int deviceId)
        {
            adapter.Connect(deviceId);
        }
    }

    public interface IDeviceCommand
    {
        void Execute();
    }

    public class TurnOnCommand : IDeviceCommand
    {
        private int deviceId;

        public TurnOnCommand(int deviceId)
        {
            this.deviceId = deviceId;
        }

        public void Execute()
        {
            DeviceAdapter adapter = new DeviceAdapter("bluetooth");
            adapter.Connect(deviceId);
            Console.WriteLine("Лампа включена!");
            // Реализация включения устройства
        }
    }

    public class TurnOffCommand : IDeviceCommand
    {
        private int deviceId;

        public TurnOffCommand(int deviceId)
        {
            this.deviceId = deviceId;
        }

        public void Execute()
        {
            DeviceAdapter adapter = new DeviceAdapter("bluetooth");
            adapter.Connect(deviceId);
            Console.WriteLine("Лампа выключена!");
            // Реализация выключения устройства
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            int deviceId = 0;
            MainDevice mainDevice = MainDevice.GetInstance();
            IDeviceCommand command = new TurnOnCommand(deviceId);
            IDeviceCommand command2 = new TurnOffCommand(deviceId); 
            mainDevice.ExecuteCommand(command);
            mainDevice.ExecuteCommand(command2);
        }
    }
}
